using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
    // public properties
    public float velocityX = 15;
    public float jumpForce = 40;
    public float jumForceGlide = 5;

    public GameObject rightBullet;
    public GameObject leftBullet;
    public GameObject piso1E;

    

    // private components
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    //private GameController game;
    
    
    private bool muere;

    // private properties
    private bool isIntangible = false;
    private float intangibleTime = 0f;

    
    // constants
    private const int ANIMATION_IDLE = 0;
    private const int ANIMATION_RUN = 1;
    private const int ANIMATION_JUMP= 2;
    private const int ANIMATION_THROW = 3;
    private const int ANIMATION_SLIDE= 4;
    private const int ANIMATION_DEAD = 5;
    private const int ANIMATION_GLIDE = 6;

    private const int LAYER_GROUND = 10;

    private const string TAG_ENEMY = "Enemy";
    private const string TAG_PISO1 = "piso1";
    private const string TAG_PISO2 = "piso2";
    private const string TAG_SUELO = "suelo";
    private const string TAG_ESCALERA = "escalera";

    private bool subir;

    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Iniciando Game Object");
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        subir = false;
        muere = false;
        //piso1E = FindObjectOfType<Piso1>();
        //game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        changeAnimation(ANIMATION_IDLE);
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y); 
            sr.flipX = false;
            changeAnimation(ANIMATION_RUN);
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocityX, rb.velocity.y);
            sr.flipX = true;
            changeAnimation(ANIMATION_RUN);
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            /*
            var bullet = sr.flipX ? leftBullet : rightBullet;
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = rightBullet.transform.rotation;
            Instantiate(bullet, position, rotation);
            */
            changeAnimation(ANIMATION_THROW);
        }
        
        if (Input.GetKey(KeyCode.X))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y);
            changeAnimation(ANIMATION_SLIDE);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // salta
            changeAnimation(ANIMATION_JUMP); // saltar
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            rb.AddForce(-Vector2.up * jumForceGlide, ForceMode2D.Impulse); // salta
            changeAnimation(ANIMATION_GLIDE); // saltar
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            //rb.AddForce(-Vector2.up * jumForceGlide, ForceMode2D.Impulse); // salta
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(0, 5);
            changeAnimation(ANIMATION_IDLE); // saltar
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        colition(collision);
        if(collision.gameObject.CompareTag(TAG_SUELO)&& muere == true){
            Debug.Log("Muere");
            changeAnimation(ANIMATION_DEAD);

            Destroy(gameObject);
        }
        if(collision.gameObject.CompareTag(TAG_ESCALERA)){
            subir = true;
        }
        /*
        if (collision.gameObject.layer == LAYER_GROUND && collision.gameObject.CompareTag("Ground"))
        {
            Debug.Log("Collision: " + collision.gameObject.name);
        }*/

        if (collision.gameObject.CompareTag(TAG_ENEMY) && !isIntangible)
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 1);
            isIntangible = true;
            //game.LoseLife();
            // Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>(), true);
        }
/*
        if (collision.gameObject.name == "Vida")
        {
            transform.localScale = new Vector3(0.7f, 0.7f, 1);
            Destroy(collision.gameObject);
        }*/
    }
    private void colition(Collision2D collision){
        if(!collision.gameObject.CompareTag(TAG_SUELO)&& !collision.gameObject.CompareTag(TAG_PISO1))
            {
                muere = true;
                Debug.Log("Muere = true");
            }
    }

    private void changeAnimation(int animation)
    {
        animator.SetInteger("estado", animation);
    }
}
